import React from "react";


import Header from "../elements/Header/Header";
import Home from "../Home/Home";

const App = () => {
      return (
         /*container div for centering content */
        <div className="container">
        <Header />
        <Home />
       
      
        <h1 className="title">React Movie Search</h1>
   
        </div>
      )
  }

  export default App