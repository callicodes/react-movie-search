import React, { Component } from "react";
import { API_URL, API_KEY, IMAGE_BASE_URL, POSTER_SIZE, BACKDROP_SIZE} from '../../config'
import HeroImage from '../elements/HeroImage/HeroImage';
import SearchBar from '../elements/SearchBar/SearchBar';
import FourColGrid from '../elements/FourColGrid/FourColGrid';
import MovieThumb from '../elements/MovieThumb/MovieThumb';
import LoadMoreBtn from '../elements/LoadMoreBtn/LoadMoreBtn';
import Spinner from '../elements/Spinner/Spinner';
import './Home.css';

/*stateful class component */
class Home extends Component{
//immutable object do not modify directly!
state={

  movies: [],
  heroImage: null,
  loading: false,
  currentPages: 0,
  searchTerm: ''

}

//lifecyle method will fire when component is mounted
//will show loading spinner first

componentDidMount(){
  this.setState({loading: true});
  // template literal below
  const endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
  this.fetchItems(endpoint);
}

searchItems = (searchTerm) => {
  console.log(searchTerm);
  let endpoint = '';
  this.setState({
    movies: [],
    loading: true,
    searchTerm
  })
 
  if(searchTerm === ''){
    endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
  }else{
    endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=en-US&query=${searchTerm}`;
  } this.fetchItems(endpoint);
}
//using 'fetch', a new cool es6 feature, also with an arrow function.
//fetch returns a promise - which wil be resolved or rejected in the future
//.then(result => result.json()) - the result is the data and it is converted to json as it is raw data
//console.log(result) to check to see if the data is coming
//Get the data into state using this.setState({})
//Will be appending the new movies to the old movies in state. Make a copy
//of the old movie state by using spread syntax then appending the movie.
// movies: [...this.state.movies, ...result.result]
// if the HeroImage returns null then it will fill it with the first movie in the API search: 
//heroImage: this.state.heroImage || result.results[0],

loadMoreItems = () => {
  let endpoint = '';
  this.setState({loading: true});

  if(this.state.searchTerm === ''){
    endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=${this.state.currentPage + 1}`;
} else {
  endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=en-US&query${this.state.searchTerm}&page=${this.state.currentPage + 1}`;
  }
  this.fetchItems(endpoint);
}
fetchItems = (endpoint) => {
  fetch(endpoint)
.then(result => result.json())
.then(result => {
  // console.log(result);
  this.setState({
    movies: [...this.state.movies, ...result.results],
    heroImage: this.state.heroImage || result.results[0],
    loading: false,
    currentPage: result.page,
    totalPages: result.total_pages
  })
})

}
render() {
    return (
        <div className="rmdb-home">
          {this.state.heroImage ? //ternary operator checking if hero image is null
          <div>
          <HeroImage 
          
          image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${this.state.heroImage.backdrop_path}`}
          title={this.state.heroImage.original_title}
          text={this.state.heroImage.overview}
          />
          <SearchBar callback={this.searchItems} />
          </div> : null}
          <div className="rmdb-home-grid">
            <FourColGrid
              header={this.state.searchTerm ? 'Search Result' : 'Popular Movies'}
              loading={this.state.loading}
              >
                {this.state.movies.map((element, i) => {
                  return <MovieThumb
                    key={i}
                    clickable={true}
                    image={element.poster_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}${element.poster_path}` : './images/no_image.jpg'}
                    movieId={element.id}
                    movieName={element.original_title}

                    />
                })}

                </FourColGrid>
          </div>
          <Spinner />
          <LoadMoreBtn />
        </div>
    )
}


}

export default Home;