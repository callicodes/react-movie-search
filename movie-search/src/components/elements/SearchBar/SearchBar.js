import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './SearchBar.css'

//font-awesome was imported with npm install
// import FontAwesome from 'react-fontawesome';

//*Searchbar - this is a controlled component, when the user types something into the box, state is auto updated.

class SearchBar extends Component {
    state = {
            value: ''//value from the input field
    }

timeout = null;

doSearch = (event) => {
    this.setState({ value: event.target.value})
    clearTimeout(this.timeout);
// we have a timeout because we don't want the search method to fire on every keystroke
    this.timeout = setTimeout( () => {
    this.props.callback(this.state.value);
    }, 500)
}

    render(){
        return (
            <div className="rmdb-searchbar">
                <div className="rmdb-searchbar-content">
                <FontAwesomeIcon className="rmdb-fa-search" name="search" size="x2" />
                <input type="text" className="rmdb-searchbar-input"
                className="rmdb-searchbar-input"
                placeholder="Search"
                onChange={this.doSearch}
                value={this.state.value}
                />
             
                </div>
                SearchBar
            </div>
        )
    }
}

export default SearchBar;